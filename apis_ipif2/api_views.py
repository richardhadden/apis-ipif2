import datetime
import json


from dateutil.parser import parse, isoparse

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ViewSet
from rest_framework import status


from apis_core.apis_entities.models import Person

from rest_framework import generics
from rest_framework import mixins

from django_filters import rest_framework as filters

from django.http import JsonResponse

from .index_sets import PersonSearchIndex


def parse_sort(string):
    if string.startswith("@"):
        string = string[1:]

    if string.endswith("ASC"):
        return string[0:-3], "ASC"
    elif string.endswith("DESC"):
        return string[0:-4], "DESC"
    else:
        return string, None


class PersonViewSet(ViewSet):
    def list(self, request):
        """Returns @id of persons matching search parameters

        ✅ $ref: '#/components/parameters/size'
        ✅ $ref: '#/components/parameters/page'
        ✅ $ref: '#/components/parameters/sortBy'
        ✅ $ref: '#/components/parameters/p'
        ✅ $ref: '#/components/parameters/factoidId'
        ✅ $ref: '#/components/parameters/f'
        ✅ $ref: '#/components/parameters/statementId'
        ✅ $ref: '#/components/parameters/st'
        ✅ $ref: '#/components/parameters/sourceId'
        ✅ $ref: '#/components/parameters/s'
        ✅ $ref: '#/components/parameters/statementText'
        ✅ $ref: '#/components/parameters/relatesToPerson'
        ✅ $ref: '#/components/parameters/memberOf'
        ✅ $ref: '#/components/parameters/role'
        ✅ $ref: '#/components/parameters/name'
        ❓ $ref: '#/components/parameters/from'
        ❓ $ref: '#/components/parameters/to'
        ✅ $ref: '#/components/parameters/place'
        """
        params = request.GET

        if personText := params.get("p"):
            # PySolaar 'manual' queries don't automatically escape colons
            personText = personText.replace(r":", r"\:")
            result = PersonSearchIndex.search(
                f"place:{personText} OR relatesToPerson:{personText} OR role:{personText} OR memberOf:{personText} OR statement_texts:{personText}"
            )

        elif factoidText := params.get("f"):
            # PySolaar 'manual' queries don't automatically escape colons
            factoidText = factoidText.replace(r":", r"\:")
            result = PersonSearchIndex.search(
                f"statement_texts:{factoidText} OR source_refs:{factoidText}"
            )
        else:
            result = PersonSearchIndex.all()

        if statementId := params.get("statementId"):
            result = result.filter(statement_refs=statementId)
        if st := params.get("st"):
            result = result.filter(statement_texts=st)
        if statementText := params.get("statementText"):
            result = result.filter(statement_texts=statementText)
        if place := params.get("place"):
            result = result.filter(place=place)
        if role := params.get("role"):
            result = result.filter(role=role)
        if relatesToPerson := params.get("relatesToPerson"):
            result = result.filter(relatesToPerson=relatesToPerson)
        if memberOf := params.get("memberOf"):
            result = result.filter(memberOf=memberOf)
        if factoidId := params.get("factoidId"):
            result = result.filter(factoidId=factoidId)

        # Sources don't have much more content to search than this at the moment
        # so point the 's' param to the source_refs field
        if sourceId := params.get("sourceId") or params.get("s"):
            result = result.filter(source_refs=sourceId)

        # Not obviously clear how we should interpret 'from' and 'to' params
        # in the context of Person filtering (here, entire lifespan must be
        # between the dates specified)
        if from_date := params.get("from"):
            from_date = parse(from_date, default=datetime.datetime(1000, 1, 1))
            result = result.filter(start_date__gte=from_date)
        if to_date := params.get("to"):
            to_date = parse(to_date, default=datetime.datetime(2022, 12, 31))
            result = result.filter(end_date__lte=to_date)

        # sorting
        if sortBy := params.get("sortBy"):
            field, direction = parse_sort(sortBy)
            if direction == "DESC":
                result.order_by(f"-{field}")
            else:
                result.order_by(field)

        # pagination
        start_pos = (int(params.get("page", 1)) - 1) * int(params.get("size", 10000000))
        result = result.paginate(start=start_pos, n=int(params.get("size", 10000000)))

        # filter return fields to just those in 'ref' set
        result = result.return_fields("ref")

        return Response(
            {
                "protocol": {
                    "size": len(result),
                    "page": int(params.get("page", 0)),
                    "totalHits": result.count(),
                },
                "persons": [
                    {
                        "@id": person.pop("id"),  # Rename "id" to "@id"
                        "factoid-refs": json.loads(person.pop("factoid-refs")),
                        **person,
                    }
                    for person in result
                ],
            }
        )

    def retrieve(self, request, pk=None):
        try:
            person = PersonSearchIndex.filter(id=pk).first()
            if not person:
                return Response(status=status.HTTP_404_NOT_FOUND)
            factoid_refs = person.pop("factoid-refs")
            data = {
                "@id": person.pop("id"),  # Rename "id" to "@id"
                **person,
                "factoid-refs": json.loads(factoid_refs),
            }
        except KeyError:
            return Response(status=status.HTTP_404_NOT_FOUND)
        except ValueError:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        return Response(data)
