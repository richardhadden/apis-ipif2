import re
from typing import Generator

from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from pysolaar import PySolaar
from reversion.models import Version

from apis_core.apis_entities.models import Person

import json
import re
import zlib

from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from rest_framework import serializers
from reversion.models import Version

from apis_bibsonomy.models import Reference

from django.conf import settings


class IPIFSearchFieldGenerator:
    def __init__(self, *args, **kwargs):
        self._source = kwargs.pop("source", None)
        self._person = kwargs.pop("person", None)
        self._attribute = kwargs.pop("attribute", None)
        # super().__init__(*args, **kwargs)


class FactoidRefGenerator(IPIFSearchFieldGenerator):
    def to_representation(self, instance, skip_fields=set()):
        res = {"@id": None}

        if "person-ref" not in skip_fields:
            res["person-ref"] = PersonRefGenerator(
                instance,
                source=self._source,
                person=self._person,
                attribute=self._attribute,
            ).to_representation(instance)
        if "source-ref" not in skip_fields:
            res["source-ref"] = SourceRefGenerator(
                instance,
                source=self._source,
                person=self._person,
                attribute=self._attribute,
            ).to_representation(instance)
        if "statement-refs" not in skip_fields:
            res["statement-refs"] = StatementRefGenerator(
                instance,
                source=self._source,
                person=self._person,
                attribute=self._attribute,
            ).to_representation(instance)

        res["@id"] = f"factoid__{instance.pk}__{res['source-ref']['@id']}"
        return res


class FactoidGenerator(IPIFSearchFieldGenerator):
    def to_representation(self, instance):
        res = super().to_representation(instance)
        # print(f"res: {res}")
        ver = Version.objects.get_for_object(instance).order_by(
            "revision__date_created"
        )
        if ver:
            res["createdBy"] = str(ver.first().revision.user)
            res["createdWhen"] = str(ver.first().revision.date_created)
            res["modifiedBy"] = str(ver.last().revision.user)
            res["modifiedWhen"] = str(ver.last().revision.date_created)
        return res


class PersonRefGenerator(IPIFSearchFieldGenerator):
    def to_representation(self, instance):
        return {"@id": instance.pk}


class PersonGenerator(PersonRefGenerator):
    def to_representation(self, instance):
        res = super().to_representation(instance)
        ver = Version.objects.get_for_object(instance).order_by(
            "revision__date_created"
        )
        res["createdBy"] = str(ver.first().revision.user)
        res["createdWhen"] = str(ver.first().revision.date_created)
        res["modifiedBy"] = str(ver.last().revision.user)
        res["modifiedWhen"] = str(ver.last().revision.date_created)
        res["uris"] = [
            str(x) for x in instance.uri_set.all().values_list("uri", flat=True)
        ]
        res["factoid-refs"] = FactoidRefGenerator(instance, source=self._source).data
        return res


class SourceRefGenerator(IPIFSearchFieldGenerator):
    def to_representation(self, instance):

        if self._source is None:
            if instance.source_id:
                return {"@id": f"original_source_{instance.source_id}"}
            else:
                return {"@id": f"original_source_for_{instance.pk}"}
        else:
            ref = Reference.objects.filter(object_id=instance.pk, bibs_url=self._source)
            self._bibref = ref
            if ref.count() != 1:
                raise ValueError("Reference does not exist for given entity.")
            else:
                return {
                    "@id": f"reference_{instance.pk}_{zlib.compress((str(instance.pk)+self._source).encode('utf8')).hex()}"
                }


class SourceGenerator(SourceRefGenerator):
    def to_representation(self, instance):
        res = super().to_representation(instance)
        if self._source is None:
            res["label"] = str(instance.source)
            res["uri"] = [
                reverse(
                    "apis:apis_api:source-detail", kwargs={"pk": instance.source_id}
                )
            ]
        else:
            if self._bibref.count() > 0:
                bibref = self._bibref.first()
            if self._bibref.count() > 1:
                print("multiple refs found")  # TODO: add propper logging
            bib = json.loads(bibref.bibtex)
            res["label"] = f"{bib['title']} ({getattr(bib, 'author', '-')})"
        ver = Version.objects.get_for_object(instance).order_by(
            "revision__date_created"
        )
        res["createdBy"] = str(ver.first().revision.user)
        res["createdWhen"] = str(ver.first().revision.date_created)
        res["modifiedBy"] = str(ver.last().revision.user)
        res["modifiedWhen"] = str(ver.last().revision.date_created)
        res["uris"] = [
            str(x) for x in instance.uri_set.all().values_list("uri", flat=True)
        ]
        res["factoid-refs"] = FactoidRefGenerator(instance, source=self._source).data
        return res


class StatementRefGenerator(IPIFSearchFieldGenerator):
    def to_representation(self, instance, include_text=False):
        r_list = []
        rels = ContentType.objects.filter(
            app_label="apis_relations", model__icontains="person"
        ).exclude(model="PersonPerson")
        if self._source:
            ref = Reference.objects.filter(bibs_url=self._source)
        for r in rels:
            if self._source:
                # print("hit correct if")
                # print(list(ref.values_list("object_id", flat=True)))
                qs_rel = r.model_class().objects.filter(
                    related_person=instance,
                    pk__in=list(ref.values_list("object_id", flat=True)),
                )
                # print(
                #    r.model_class()
                #    .objects.filter(related_person=instance)
                #    .values_list("pk", flat=True)
                # )
            else:
                qs_rel = r.model_class().objects.filter(related_person=instance)
            for rr in qs_rel:
                item = {"@id": f"{instance.pk}_{r.model_class().__name__}_{rr.pk}"}
                if include_text:
                    item["text"] = str(rr)
                r_list.append(item)
                # print(f"r_list: {r_list}")
        for f in instance._meta.fields:
            # print(f)
            field_name = re.search(r"([A-Za-z]+)\'>", str(f.__class__)).group(1)
            if field_name in [
                "CharField",
                "DateField",
                "DateTimeField",
                "IntegerField",
                "FloatField",
                "ForeignKey",
            ] and f.name.lower() not in ["source", "status"]:
                if self._source:
                    if f.name in list(ref.values_list("attribute", flat=True)):
                        item = {"@id": f"{instance.pk}_attrb_{f.name}"}
                        if include_text:
                            item["text"] = f.value_to_string(instance)
                        r_list.append(item)
                else:
                    item = {"@id": f"{instance.pk}_attrb_{f.name}"}
                    if include_text:
                        item["text"] = f.value_to_string(instance)
                    r_list.append(item)
        for f in instance._meta.many_to_many:
            if str(f.related_model.__module__).endswith(
                "apis_vocabularies.models"
            ) and not f.name.endswith("set"):
                if self._source:
                    if f.name not in list(ref.values_list("attribute", flat=True)):
                        continue
                for x1 in getattr(instance, f.name).all():
                    item = {"@id": f"{instance.pk}_m2m_{f.name}_{x1.pk}"}
                    if include_text:
                        item["text"] = str(x1)
                    r_list.append(item)
        return r_list


class StatementGenerator(StatementRefGenerator):
    def to_representation(self, instance):
        res = super().to_representation(instance)
        print(res)
        r_list = []
        ver = Version.objects.get_for_object(instance).order_by(
            "revision__date_created"
        )
        res2 = dict()
        if ver.count() > 0:
            res2["createdBy"] = str(ver.first().revision.user)
            res2["createdWhen"] = str(ver.first().revision.date_created)
            res2["modifiedBy"] = str(ver.last().revision.user)
            res2["modifiedWhen"] = str(ver.last().revision.date_created)
        res2["factoid-refs"] = FactoidRefGenerator(
            instance, source=self._source
        ).to_representation(instance)
        for r1 in res:
            print(r1)
            r1 = dict(r1, **res2)
            if "_attrb_" in r1["@id"]:
                print(r1)
                d1 = r1["@id"].split("_attrb_")
                r1["role"] = {"label": d1[1]}
                r1["statementType"] = {"label": getattr(instance, d1[1], "-")}
            r_list.append(r1)

        return r_list


class PersonSearchIndex(PySolaar):
    default_search_field = "text"
    return_fields = {
        "default": (
            "label",
            "uris",
            "factoid-refs",
            "createdBy",
            "createdWhen",
            "modifiedBy",
            "modifiedWhen",
        ),
        "ref": {"label", "uris", "factoid-refs"},
    }
    single_value_fields = {
        "label",
        "factoid-refs",
        "createdBy",
        "createdWhen",
        "modifiedBy",
        "modifiedWhen",
    }

    def build_person_doc(person: Person) -> dict:
        """ Constructs a person index document from an APIS Person entity """
        doc = {}
        ver = Version.objects.get_for_object(person).order_by("revision__date_created")
        doc["id"] = person.pk
        doc["label"] = f"{person.name}, {person.first_name}"
        doc["uris"] = [
            str(x) for x in person.uri_set.all().values_list("uri", flat=True)
        ]
        if ver:
            doc["createdBy"] = str(ver.first().revision.user)
            doc["createdWhen"] = str(ver.first().revision.date_created)
            doc["modifiedBy"] = str(ver.last().revision.user)
            doc["modifiedWhen"] = str(ver.last().revision.date_created)

        doc["factoid-refs"] = ["something"]
        doc["name"] = [person.name, person.first_name]

        doc["relatesToPerson"] = set()
        for related_person in person.get_related_person_instances():
            doc["relatesToPerson"].add(related_person.name)
            doc["relatesToPerson"].add(related_person.first_name)
            [
                doc["relatesToPerson"].add(str(x))
                for x in related_person.uri_set.all().values_list("uri", flat=True)
            ]

        doc["memberOf"] = set()
        for institution in person.institution_set.all():
            doc["memberOf"].add(institution.name)
            [
                doc["memberOf"].add(str(x))
                for x in institution.uri_set.all().values_list("uri", flat=True)
            ]

        # Role should index professions, and roles within institutions,  person-relation types, and
        # TODO: event roles?
        # (using set as easy way to avoid duplication)
        doc["role"] = (
            {profession.name for profession in person.profession.all()}
            | {str(a.relation_type) for a in person.personinstitution_set.all()}
            | {str(rel) for rel in person.personA_relationtype_set.all()}
            | {str(rel) for rel in person.personB_relationtype_set.all()}
        )

        doc["place"] = set()
        # The place field should be searchable by label (i.e. place name) and url
        # of place if there is one
        for personplace in person.personplace_set.all():
            place = personplace.related_place
            doc["place"].add(place.name)
            [doc["place"].add(str(url)) for url in place.uri_set.all()]

        # Assume these are dates of birth and death for person
        doc["start_date"] = person.start_date
        doc["end_date"] = person.end_date

        statements_dict = StatementRefGenerator(person=person).to_representation(
            person, include_text=True
        )

        doc["statement_refs"] = [
            statement["@id"] for statement in statements_dict if statement["text"]
        ]
        doc["statement_texts"] = [statement["text"] for statement in statements_dict]

        references = Reference.objects.filter(object_id=person.pk)
        # doc["source_refs"] = [ref.bibs_url for ref in references]

        factoid_refs = [
            FactoidRefGenerator(
                person=person, source=getattr(ref, "bibs_url", None)
            ).to_representation(person, skip_fields=("person-ref",))
            for ref in [*references, None]
        ]
        # Searcheable factoid id field
        doc["factoidId"] = [f["@id"] for f in factoid_refs]

        # Factoid data returned (un JSON'd on return)
        doc["factoid-refs"] = json.dumps(factoid_refs)

        references = Reference.objects.filter(object_id=person.pk)

        doc["source_refs"] = [ref.bibs_url for ref in references] + [
            fr["source-ref"]["@id"] for fr in factoid_refs
        ]

        return doc

    def get_index_set(self) -> Generator:
        for person in Person.objects.iterator():  # .filter(pk=141689)
            doc = self.build_person_doc(person)
            yield doc


# "http://localhost:8983/solr/test_solr"

try:
    PySolaar.configure_solr(settings.IPIF_SOLR_CORE_URL, always_commit=True)
except:
    # raise Exception("eexxxx")
    exit("APIS_IPIF configuration error: settings.IPIF_SOLR_CORE_URL missing")
# print("Starting PySolaar update")
# PySolaar.update(max_chunk_size=200)

# print("PySolaar update complete")

