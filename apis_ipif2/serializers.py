from rest_framework import serializers
from reversion.models import Version

from apis_core.apis_entities.models import Person

# Let's see whether we can use this serializer for
class PersonSerializer(serializers.BaseSerializer):
    def to_representation(self, instance):
        resp = {}
        ver = Version.objects.get_for_object(instance).order_by(
            "revision__date_created"
        )

        resp["@id"] = instance.id
        resp["uris"] = [
            str(x) for x in instance.uri_set.all().values_list("uri", flat=True)
        ]
        resp["createdBy"] = str(ver.first().revision.user)
        resp["createdWhen"] = str(ver.first().revision.date_created)
        resp["modifiedBy"] = str(ver.last().revision.user)
        resp["modifiedWhen"] = str(ver.last().revision.date_created)
        resp["factoid-refs"] = ["something"]
        return resp


class PersonRefSerializer(serializers.BaseSerializer):
    def to_representation(self, instance):
        resp = {}
        resp["@id"] = instance.id
        return resp
