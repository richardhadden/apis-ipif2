from django.urls import path

from . import api_views

app_name = "ipif"

urlpatterns = [
    path("persons/", api_views.PersonViewSet.as_view({"get": "list"})),
    path("person/<int:pk>/", api_views.PersonViewSet.as_view({"get": "retrieve"})),
]
